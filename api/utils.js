'use strict';

var http = require('http');
var Xray = require("x-ray");

var opcoesDePesuisa = ["NUMPROC", "NMPARTE", "DOCPARTE", "NMADVOGADO", "NUMOAB", "PRECATORIA", "DOCDELEG", "NUMCDA"];

var xray = Xray({
    filters: {
        cleanComponent: (value) => {
            return typeof value === 'string' ? value.replace(/[\n\t\r]/g, '').trim() : value
        },
        whiteSpaces: (value) => {
            return typeof value === 'string' ? value.replace(/ +/g, ' ').trim() : value
        }
    }
});

function requisicao(req, res, url) {
    http.get(url, (resp) => {
        console.log('statusCode: ', resp.statusCode);

        if (resp.statusCode !== 200 && resp.statusCode !== 302) {
            res.status(resp.statusCode).json({
                message: `Request Failed.\n Status Code: ${resp.statusCode}`
            });
            resp.resume();
            return;
        }

        resp.setEncoding('utf8');
        var rawData = '';
        resp.on('data', (chunk) => {
            rawData += chunk;
        });
        resp.on('end', () => {
            try {
                var regex = /listagemDeProcessos/i;
                if (regex.test(rawData)) {
                    pesquisarProcessos(req, res, rawData);
                } else {
                    pesquisarProcesso(req, res, url);
                }
            } catch (e) {
                console.log(e.message);
                res.status(404).json({
                    message: e.message
                });
            }
        });
    }).on('error', (error) => {
        console.log(error.message);
        res.status(404).json({
            message: error.message
        });
    });
};

function pesquisarProcesso(req, res, rawData) {
    var scope = '.esajCelulaConteudoServico'
    var selector = {
        dadosProcesso: xray('div table[id=""].secaoFormBody tr', [{
            dado: 'td:nth-child(1)[width="150"] | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        partesPrincipais: xray('#tablePartesPrincipais .fundoClaro', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        todasPartes: xray('#tableTodasPartes .fundoClaro', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(2) | cleanComponent'
        }]),
        ultimasMovimentacoes: xray('#tabelaUltimasMovimentacoes tr', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(3) | cleanComponent'
        }]),
        todasMovimentacoes: xray('#tabelaTodasMovimentacoes tr', [{
            dado: 'td:nth-child(1) | cleanComponent',
            valor: 'td:nth-child(3) | cleanComponent'
        }]),
        historicoDeClasses: xray('#tdHistoricoDeClasses', [
            ['tr | cleanComponent']
        ]),
        seccoes: xray('div[style="padding: 10px 0 10px 0;"]', [
            ['tr | cleanComponent']
        ]),
        diversas: xray('table[style="margin-left:15px; margin-top:1px;"]', [
            ['tr | cleanComponent']
        ])
    };
    xray(rawData, scope, selector)
        ((err, obj) => {
            if (err && Object.keys(err) > 0) {
                res.status(404).json({
                    message: err.message
                });
            } else if (validarRetorno(obj)) {
                obj.diversas = obj.diversas.filter(data => data.length > 0 && (data.length == 1 || data[1]));
                var peticoesDiversas, incidentesAcoesIncidentaisRecursosExecucoes, audiencias, historicoDeClasses;
                var diversas = [];
                for (var index = 0; index < obj.seccoes.length; index++) {
                    var seccao = obj.seccoes[index];
                    if (seccao && seccao.indexOf('Partes do processo') !== -1) {
                        // TODO
                    } else if (seccao && seccao.indexOf('Movimentações') !== -1) {
                        // TODO
                    } else if (seccao && seccao.indexOf('Petições diversas') !== -1) {
                        peticoesDiversas = obj.diversas[index];
                    } else if (seccao && seccao.indexOf('Incidentes, ações incidentais, recursos e execuções de sentenças') !== -1) {
                        incidentesAcoesIncidentaisRecursosExecucoes = obj.diversas[index];
                    } else if (seccao && seccao.indexOf('Audiências') !== -1) {
                        audiencias = obj.diversas[index];
                    } else if (seccao && seccao.indexOf('Histórico de classes') !== -1) {
                        historicoDeClasses = obj.diversas[index];
                    }
                    diversas.push({
                        dado: obj.seccoes[index][0],
                        valor: obj.diversas[index]
                    });
                }
                res.json(Object.assign({
                    dadosProcesso: obj.dadosProcesso,
                    partesPrincipais: obj.partesPrincipais,
                    todasPartes: obj.todasPartes,
                    ultimasMovimentacoes: obj.ultimasMovimentacoes,
                    todasMovimentacoes: obj.todasMovimentacoes,
                    peticoesDiversas: peticoesDiversas,
                    incidentesAcoesIncidentaisRecursosExecucoes: incidentesAcoesIncidentaisRecursosExecucoes,
                    audiencias: audiencias,
                    historicoDeClasses: historicoDeClasses,
                    diversas: diversas
                }));
            } else {
                res.status(204).json({
                    message: 'Processo não encontrado.'
                });
            }
        });
};

function pesquisarProcessos(req, res, rawData) {
    var scope = '.esajCelulaConteudoServico div[id="listagemDeProcessos"]';
    var selector = {
        processos: xray('div[id^="divProcesso"] div[class^="fundo"]', [{
            url: 'div.nuProcesso a@href',
            numero: 'div.nuProcesso a | cleanComponent',
            processo: 'div.nuProcesso | cleanComponent',
            infomacoes: ['div.espacamentoLinhas | cleanComponent']
        }])
    };
    xray(rawData, scope, selector)
        ((err, obj) => {
            if (err && Object.keys(err) > 0) {
                res.status(404).json({
                    message: err.message
                });
            } else if (validarRetorno(obj)) {
                res.json(obj.processos);
            } else {
                res.status(204).json({
                    message: 'No Content.'
                });
            }
        });
};

function validarRetorno(obj) {
    var key = Object.keys(obj).find(key => key.indexOf('dadosProcesso') !== -1 || key.indexOf('processos') !== -1);
    return (obj && obj[key] && obj[key].length > 0);
};

function validarFiltro(filtro) {
    var opcaoPesquisa = filtro && filtro['opcaoPesquisa'] && opcoesDePesuisa.find(opcao => opcao.indexOf(filtro['opcaoPesquisa']) !== -1) || false;
    var valorConsulta = filtro && filtro['valorConsulta'] && filtro['valorConsulta'].length > 0 || false;
    return opcaoPesquisa && valorConsulta;
};

function pesquisar(req, res, filtro) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    var url = 'http://esaj.tjsp.jus.br/cpopg/search.do?' +
        'conversationId=' +
        '&dadosConsulta.localPesquisa.cdLocal=-1' +
        '&cbPesquisa=' + filtro.opcaoPesquisa +
        '&dadosConsulta.tipoNuProcesso=' +
        '&numeroDigitoAnoUnificado=' +
        '&foroNumeroUnificado=' +
        '&dadosConsulta.valorConsultaNuUnificado=' +
        '&dadosConsulta.valorConsulta=' + encodeURIComponent(filtro.valorConsulta).replace(/%20/g, '+') +
        '&uuidCaptcha=' +
        '&pbEnviar=Pesquisar';
    if (filtro.opcaoPesquisa.indexOf('NUMPROC') !== -1) {
        pesquisarProcesso(req, res, url);
    } else {
        requisicao(req, res, url);
    }
};

module.exports = {
    validarFiltro,
    pesquisar
}