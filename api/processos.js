'use strict';

const express = require('express');
const utils = require('./utils');

const router = express.Router();

function processarRequisicao(req, res, filtro) {
    if (utils.validarFiltro(filtro)) {
        utils.pesquisar(req, res, filtro);
    } else {
        res.status(400).json({
            message: 'Bad Request.'
        });
    }
}

router.get('/', (req, res) => {
    console.log('GET: /', 'index.html');
    res.sendFile(`${__dirname}/index.html`)
})

router.get('/:numProcesso', function (req, res) {
    console.log('GET: /', '/:processo');
    const _numProcesso = req.params.numProcesso;
    const filtro = {
        opcaoPesquisa: 'NUMPROC',
        valorConsulta: _numProcesso
    };
    processarRequisicao(req, res, filtro);
});

router.get('/:opcaoPesquisa/:valorConsulta', function (req, res) {
    console.log('GET: /:opcaoPesquisa/:valorConsulta', JSON.stringify(req.params));
    const _opcaoPesquisa = req.params.opcaoPesquisa;
    const _valorConsulta = req.params.valorConsulta;
    const filtro = {
        opcaoPesquisa: _opcaoPesquisa,
        valorConsulta: _valorConsulta
    };
    processarRequisicao(req, res, filtro);
});

router.post('/', function (req, res) {
    console.log('POST: /', JSON.stringify(req.body));
    const filtro = req.body
    processarRequisicao(req, res, filtro);
});

module.exports = router;