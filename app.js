'use strict';

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

const app = express();
const hostname = 'localhost';
const port = 3001;

app.set("json spaces", 4);

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(awsServerlessExpressMiddleware.eventContext())

app.use('/api/processos', require('./api/processos'));

// The aws-serverless-express library creates a server and listens on a Unix
// Domain Socket for you, so you can remove the usual call to app.listen.
// const server = app.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });

// Export your express server so you can import it in the lambda function.
module.exports = app